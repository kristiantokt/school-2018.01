prefix=/gh

install: ${prefix}
	tar cf - . | (cd ${prefix} && tar xfp -)

requirements: requirements.txt
	-pip install -r requirements.txt

apt-get:
	-apt-get install git python3 python3-pip

${prefix}:
	mkdir -p ${prefix}

.PHONY: install requirements apt-get
