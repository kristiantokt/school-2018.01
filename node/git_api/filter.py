import subprocess
from subprocess import PIPE
import os
import shutil


class GitRepositoryDownloader:
    def __init__(self, link):
        subprocess.run(["git", "clone", "--depth=1", link], stdout=subprocess.PIPE)
        arr = link.split("/")
        self._repository_name = arr[-1]
        self._files_of_the_dir = []
        self._direct_name = self._repository_name + "." + arr[-2]

    def  _process_directory(self, dir_name):
        files_direct = os.listdir(dir_name)
        for f in files_direct:
            next_f = os.path.join(dir_name,f)
            if not f.endswith(".py"):
                if os.path.isdir(next_f):
                    self. _process_directory(next_f)
                    if len(os.listdir(path=next_f)) == 0:
                        os.rmdir(next_f)
                else:
                    os.remove(next_f)
            else:
                self._files_of_the_dir.append(next_f)

    def run(self):
        self._process_directory(self._repository_name)
        os.rename(self._repository_name, self._direct_name)
        return self._files_of_the_dir
